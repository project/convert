This module makes it easy to add the Convert.com tracking code in your page's HEAD.

Convert.com is an A/B testing and multivariate testing tool that helps you optimize your site's conversion rate.

Enable the module, go to the settings page (admin/config/system/convert), set it to ON and insert your account id. Then, you are set to go.

This module is created and sponsored by Netstudio.gr, a drupal services company based in Athens, Greece.